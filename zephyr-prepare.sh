#
# Prepare zephyr
#
# This script will build a dummy zephyr application.
# Step 1: cmake is called in order to create the ninja-build file
# Step 2: Compile/link flags gets extracted with a python script.
# Step 3: The zephyr project is build (dummy project)
# Step 4: The 'zephyr-finish.sh' script is generated to produce a merged hex file
# Step 5: The empty dummy header file is created
#
# Parameters:
# $1 - Workspace root directory
# $2 - Workspace build directory (not containing subdirectory name)
# $3 - emBase_zephyr main directory (not containing subdirectory name)
# $4 - CPU name (used as board name)
# $5 - Absolute filename 'libZephyr.args' (compiler arguments)
# $6 - Absolute filename 'libZephyr.incargs' (include directories)
# $7 - Absolute filename 'libZephyr.linkargs' (linker arguments)
# $8 - Absolute filename 'libZephyr.post' (post processing)
# $9 - dummy header (must be touched)
#
# Clean build directory
cd $2 && rm -rf libZephyr && mkdir libZephyr
#
# Copy template to libZephyr
cd $1/subprojects/emBase_zephyr && rm -rf libZephyr && mkdir libZephyr && cp -r $1/subprojects/emBase_zephyr/template/* libZephyr/
#
# Copy emBase_config/zephyr
if [ -d "$1/subprojects/emBase_config/zephyr" ]
then
	cp -r $1/subprojects/emBase_config/zephyr/* $1/subprojects/emBase_zephyr/libZephyr/
fi
#
# Execute cmake and generate ninja files
export ZEPHYR_SDK_INSTALL_DIR=/home/user/external/zephyr-sdk
export ZEPHYR_BASE=/home/user/external/ncs/zephyr
export Zephyr_DIR=${ZEPHYR_BASE}/share/zephyr-package/cmake
cd $3 && cmake -GNinja -B$2/libZephyr -S$3/libZephyr -DBOARD=$4
#
# Extract flags
python3 zephyr-ninja-scan.py $2/libZephyr $5 $6 $7 $8
#
# Run ninja
cd $2/libZephyr && ninja
#
# Link finish script
rm -f $2/zephyr-finish.sh
ln -s $3/zephyr-finish.sh $2/zephyr-finish.sh
#
# Dummy header 
rm -f $9 && touch $9
#
