#
# Scan ninja build files
#
# Parameters
#	$1 - Project build directory
#   $2 - Output compile arguments
#   $3 - Output include arguments
#   $4 - Output linker arguments
#   $5 - Output postprocess arguments
#
# Output files
#   libZephyr.args
#   libZephyr.incargs
#   libZephyr.linkargs
#
import sys

# Check if line starts with specified strings and write all arguments to file
def write_args (file, start, line):
    if line.startswith(start):
        slen= len(start)
        line2= line [slen:]
        for part in line2.split():
            if part[0]!='/' and part[0]!='-':
                part= build + "/" + part
            file.write(part + "\n")

def write_line (file, start, line):
    if line.startswith(start):
        slen= len(start)
        line2= line [slen:]
        file.write(line2)

def write_inc_add (file):
	file.write("-I/home/user/external/ncs/modules/tee/tf-m/trusted-firmware-m/interface/include\n")

# Read build.ninja
build= sys.argv[1]
build_ninja= open (build + "/build.ninja", "r")
lines= build_ninja.readlines()
build_ninja.close()

# Create output files
output_args     = open (sys.argv[2], "w")
output_incargs  = open (sys.argv[3], "w")
output_linkargs = open (sys.argv[4], "w")
output_post     = open (sys.argv[5], "w")

# Linker path
output_linkargs.write("-L\"" + build + "\"\n")

# Scan file
index= 0
for line in lines:
    index+= 1
    if line.startswith("build CMakeFiles/app.dir/src/main.c.obj:"):
        write_args (output_args, "  DEFINES = ", lines[index])
        write_args (output_args, "  FLAGS = ", lines[index+2])
        write_args (output_incargs, "  INCLUDES = ", lines[index+3])
        write_inc_add (output_incargs)
    if line.startswith("build zephyr/zephyr.elf "):
        write_args (output_linkargs, "  LINK_FLAGS = ", lines[index])
        write_args (output_linkargs, "  LINK_LIBRARIES = ", lines[index+1])
        write_line (output_post, "  POST_BUILD = ", lines[index+3])

# Close files
output_args.close()
output_incargs.close()
output_linkargs.close()
output_post.close ()
