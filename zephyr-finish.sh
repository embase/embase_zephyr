#
# Finish zephyr build
#
# This script should be called after the zephyr application has been
# built.
# Step 1: Copy the application to the zephyr build tree
# Step 2: Fiish building zephyr
# Step 3: Copy resulting and merged hex file
#
# $1 - Absolute filename of zephyr application
# $2 - Workspace build directory (not containing subdirectory name)
# $3 - Absolute filename of merged application as HEX file
#
# Copy
cp $1 $2/libZephyr/app/libapp.a
#
# Postprocessing
# . $2/libZephyr/libZephyr.post
#
# Rebuild
cd $2/libZephyr && ninja
#
# Result
echo "Copy to $3"
cp $2/libZephyr/zephyr/zephyr.elf $3.elf
cp $2/libZephyr/zephyr/merged.hex $3.hex
#