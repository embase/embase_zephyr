option(
	'EB_ARCH', 
    type : 'combo', 
    choices : [ 'EB_ARCH_NRFX' ], 
    value: 'EB_ARCH_NRFX', 
    yield : true, 
    description : 'The architecture the emBase is targeting'
)
option(
	'EB_OS', 
    type : 'combo', 
    choices : ['EB_OS_ZEPHYR'],
    value: 'EB_OS_ZEPHYR', 
    yield : true, 
    description : 'The operating system the emBase is targeting'
)
option(
	'EB_TRACE_RTT', 
    type: 'boolean', 
    value: false, 
	yield: true, 
    description: 'Activate log output through SEGGER RTT'
)
option(
	'EB_TRACE_SYSVIEW', 
    type: 'boolean', 
    value: false, 
    yield: true, 
    description: 'Activate logging with SEGGER SystemView'
)
option(
	'EB_CPU', 
    type : 'string', 
    value: 'nrf9160',
    yield : true, 
    description : 'CPU type (might be used in architecture layer)'
)
option(
    'EB_APP_NAME',
    type: 'string',
    yield : true,
    description: 'Application name (might be used in architecture layer)'
)
option(
	'EB_MEMALLOC',
    type : 'combo', 
    choices : ['EB_MEMALLOC_OS', 'EB_MEMALLOC_TLSF', 'EB_MEMALLOC_O1HEAP'],
    value: 'EB_MEMALLOC_TLSF',
    yield : true, 
	description: 'Memory allocator implementation'
)
option(
	'EB_BUILD_DOC',
	type: 'boolean',
	value: false,
	yield: true,
	description: 'Flag if documentation is built in default build'
)
