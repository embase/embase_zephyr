if(CONFIG_BOARD_EMBASE_NRF9160 AND NOT DEFINED CONFIG_MCUBOOT)
	zephyr_library()
endif()

if(CONFIG_BOARD_EMBASE_NRF9160_NS)
	if(CONFIG_BUILD_WITH_TFM)
		zephyr_library()
	endif()

	# Use static partition layout to ensure the partition layout remains
	# unchanged after DFU. This needs to be made globally available
	# because it is used in other CMake files.
	if (CONFIG_SPM)
		set(PM_STATIC_YML_FILE ${CMAKE_CURRENT_LIST_DIR}/embase_pm_static_spm.yml CACHE INTERNAL "")
	elseif(CONFIG_LWM2M_CARRIER)
		set(PM_STATIC_YML_FILE ${CMAKE_CURRENT_LIST_DIR}/embase_pm_static_lwm2m_carrier.yml CACHE INTERNAL "")
	else()
		set(PM_STATIC_YML_FILE ${CMAKE_CURRENT_LIST_DIR}/embase_pm_static.yml CACHE INTERNAL "")
	endif()
endif()
